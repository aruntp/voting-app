import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './view/home/home.component';
import { FormsModule } from '@angular/forms';
import { ComponentModule } from './components/component.module';

@NgModule({
  declarations: [
    HomeComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ComponentModule
  ],
  exports: [
    HomeComponent
  ]
})
export class VotingModule { }
