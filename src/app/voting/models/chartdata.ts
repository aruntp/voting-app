/**
 * A cool interface to use as model for chartData
 */
 export interface ChartData {
    label: string;
    count: number;
  }
  