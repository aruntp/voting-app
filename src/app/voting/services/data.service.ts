import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  voteEmitter = new Subject<string>();
  answerAddEvent = new Subject<string>();
  answerRemoveEvent = new Subject<string>();
  resetEvent = new Subject<boolean>();

  constructor() { }
}
