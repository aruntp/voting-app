export const answers = [
    '3.14', '3.1454', '3.125', '3.25', '3.65'
]

export const updatedAnswers = [
    '3.14', '3.1454', '3.125', '3.25', '3.65', '3.30'
]

export const chartData = [
    {label: '3.14', count: 0},
    {label: '3.1454', count: 0},
    {label: '3.125', count: 0},
    {label: '3.25', count: 0},
    {label: '3.65', count: 0},
]

export const updatedChartData = [
    {label: '3.14', count: 0},
    {label: '3.1454', count: 0},
    {label: '3.125', count: 0},
    {label: '3.25', count: 0},
    {label: '3.65', count: 0},
    {label: '3.30', count: 0},
]