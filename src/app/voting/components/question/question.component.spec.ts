import { ComponentFixture, TestBed } from '@angular/core/testing';
import { updatedAnswers, answers } from '../../mock-data/test-data';

import { QuestionComponent } from './question.component';

describe('QuestionComponent', () => {
  let component: QuestionComponent;
  let fixture: ComponentFixture<QuestionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuestionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('reset everything on reset button click', () => {
    component.resetForm();
    expect(component.answer).toEqual('');
    expect(component.question).toEqual('');
    expect(component.answers).toEqual([]);
  });

  it('should add answer on answeradd event', () => {
    component.answers = answers;
    component.answer = '3.30';
    component.addAnswer();
    expect(component.answers).toEqual(updatedAnswers);
  });

  it('should remove answer on answeradd event', () => {
    component.answers = updatedAnswers;
    component.removeAnswer('3.30');
    expect(component.answers).toEqual(answers);
  });

});
