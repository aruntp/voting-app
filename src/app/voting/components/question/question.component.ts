import { Component, EventEmitter, Output } from '@angular/core';
import { DataService } from '../../services/data.service';


@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent {

  answer: string = '';
  answers: string[] = [];
  question: string = '';
  
  @Output() questionEvent = new EventEmitter<string>();

  constructor(private dataSrvc: DataService) { }

  /**
   * Add answer to the answers array, which is shared across components
   * Emits event so that the chart component can prepare the chart data
   * @returns without doing anything, if answer is already there, or no answer
   */
  addAnswer() {
    if (this.answers.includes(this.answer) || this.answer.length == 0) {
      alert("The answer is already present, or add a valid answer");
      return;
    }
    this.answers.push(this.answer);
    this.dataSrvc.answerAddEvent.next(this.answer);
    this.answer = '';
  }

  /**
   * Remove the answer from the answers array and notify the chart component about the removal
   * @param ans - answer to be removed
   */
  removeAnswer(ans: string) {
    const index = this.answers.indexOf(ans);
    if (index > -1) {
      this.answers.splice(index, 1);
      this.dataSrvc.answerRemoveEvent.next(ans);
    }
  }

  /**
   * Reset everything on reset button click
   */
  resetForm() {
    this.answer = '';
    this.answers = [];
    this.question = '';
    this.dataSrvc.resetEvent.next(true);
  }

  /**
   * Emits the question event with question passed as parameter
   */
  questionAdded() {
    this.questionEvent.emit(this.question);
  }

}


