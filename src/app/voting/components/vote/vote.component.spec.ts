import { ComponentFixture, TestBed } from '@angular/core/testing';
import { answers, updatedAnswers } from '../../mock-data/test-data';

import { VoteComponent } from './vote.component';

describe('VoteComponent', () => {
  let component: VoteComponent;
  let fixture: ComponentFixture<VoteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VoteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should reset everything on reset', () => {
    component.resetAll()
    expect(component.answers).toEqual([]);
    expect(component.question).toEqual('');
  });

  it('should unsubscribe to events ', () => {
    spyOn(component['resetEventSubscripton'], 'unsubscribe');
    spyOn(component['answerAddEventSubscription'], 'unsubscribe');
    spyOn(component['answerRemoveEventSubscription'], 'unsubscribe');
    component.ngOnDestroy();
    expect(component['resetEventSubscripton'].unsubscribe).toHaveBeenCalledTimes(1);
    expect(component['answerAddEventSubscription'].unsubscribe).toHaveBeenCalledTimes(1);
    expect(component['answerRemoveEventSubscription'].unsubscribe).toHaveBeenCalledTimes(1);
  })

  it('should remove answer on answeradd event', () => {
    component.answers = updatedAnswers;
    component.removeAnswers('3.30');
    expect(component.answers).toEqual(answers);
  });

  it('should add answer on answeradd event', () => {
    component.answers = answers;
    component.updateAnswers('3.30');
    expect(component.answers).toEqual(updatedAnswers);
  });

  it('should update answer on vote event', () => {
    component.updateAnswer('3.30');
    expect(component.answer).toEqual('3.30');
  });

});
