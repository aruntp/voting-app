import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-vote',
  templateUrl: './vote.component.html',
  styleUrls: ['./vote.component.scss']
})
export class VoteComponent implements OnInit, OnDestroy {
  
  answer: string = "";
  answers: string[] = [];
  votedAnswer: any;

  @Input() question: string = '';

  private answerAddEventSubscription!: Subscription;
  private answerRemoveEventSubscription!: Subscription;
  private resetEventSubscripton!: Subscription;

  constructor(private dataSrvc: DataService) { }

  /**
   * Inform the chart component about the vote action through event, and pass the answer as parameter
   */
  castVote() {
    this.dataSrvc.voteEmitter.next(this.votedAnswer)
  }

  /**
   * Unsubscribe to all subscribed events 
   */
  ngOnDestroy(): void {
    this.answerAddEventSubscription.unsubscribe();
    this.answerRemoveEventSubscription.unsubscribe();
    this.resetEventSubscripton.unsubscribe();
  }

  /**
   * Subscribe to the events from question component and use the handler
   */
  ngOnInit(): void {
    this.answerAddEventSubscription = this.dataSrvc.answerAddEvent.subscribe((answer: string) => {
      this.updateAnswers(answer);
    });
    this.answerRemoveEventSubscription = this.dataSrvc.answerRemoveEvent.subscribe((answer: string) => {
      this.removeAnswers(answer);
    });
    this.resetEventSubscripton = this.dataSrvc.resetEvent.subscribe(reset => {
      if (reset) {
        this.resetAll();
      }
    })
  }

  /**
   * Remove the answer from the answers array
   * @param ans - answer to be removed
   */
   removeAnswers(ans: any) : void {
    const index = this.answers.indexOf(ans);
    if (index > -1) {
      this.answers.splice(index, 1);
      this.dataSrvc.answerRemoveEvent.next(ans);
    }
  }

  /**
   * Reset the answers and question
   */
  resetAll() {
    this.answers = [];
    this.question = '';
  }

  /**
   * Store the answer to be passed to the event
   * @param ans - the voted answer
   */
  updateAnswer(ans: any): void {
    this.votedAnswer = ans;
  }

  /**
   * Add the answer to the answers array used to display the voting options
   * @param ans - the voted answer
   */
  updateAnswers(ans: any): void {
    this.answers.push(ans);
  }

}
