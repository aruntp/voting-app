import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { VoteComponent } from './vote/vote.component';
import { QuestionComponent } from './question/question.component';
import { ChartComponent } from './chart/chart.component';

@NgModule({
  declarations: [
    ChartComponent,
    VoteComponent,
    QuestionComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
  ],
  exports: [
    ChartComponent,
    VoteComponent,
    QuestionComponent,
  ]
})
export class ComponentModule { }