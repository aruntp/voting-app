import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ChartData } from '../../models/chartdata';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit, OnDestroy {

  chartData: ChartData[] = [];
  totalVotes: number = 0;
  answers: string [] = [];

  @Input() question: string = "";

  private answerAddEventSubscription!: Subscription;
  private answerRemoveEventSubscription!: Subscription;
  private resetEventSubscripton!: Subscription;
  private voteSubscription!: Subscription;

  constructor(private dataSrvc: DataService) { }

  /**
   * Increment the count for the voted answer
   * @param answer - The answer which user has voted, which is passed through the event
   */
  incrementVote(answer: string) {
    const obj = this.chartData.find(ans => (ans.label == answer))
    if (obj) {
      obj.count++;
      this.totalVotes++;
    }
  }

  /**
   * Clean up everything
   */
  ngOnDestroy() {
    this.voteSubscription.unsubscribe();
    this.answerAddEventSubscription.unsubscribe();
    this.answerRemoveEventSubscription.unsubscribe();
    this.resetEventSubscripton.unsubscribe();
  }

  /**
   * Subscribe to all events from other components, and invoke the handler methods
   */
  ngOnInit(): void {
    this.voteSubscription = this.dataSrvc.voteEmitter.subscribe((answer: string) => {
      this.incrementVote(answer);
    });
    this.answerAddEventSubscription = this.dataSrvc.answerAddEvent.subscribe((answer: string) => {
      this.updateChartData(answer);
    });
    this.answerRemoveEventSubscription = this.dataSrvc.answerRemoveEvent.subscribe((answer: string) => {
      this.removeFromChartData(answer);
    });
    this.resetEventSubscripton = this.dataSrvc.resetEvent.subscribe(reset => {
      if (reset) {
        this.resetAll();
      }
    })
  }

  /**
   * Remove the answer from chart data
   * @param answer - the answer which is removed by the user from the question component
   */
  removeFromChartData(answer: string) {
    const index = this.chartData.findIndex(data => data.label == answer);
    const data = this.chartData.find(data => data.label == answer);
    if (index >= 0) {
      const count = data ? data.count : 0;
      this.totalVotes = this.totalVotes - count;
      this.chartData.splice(index, 1);
    }
  }

  /**
   * when the reset button is clicked from question component, reset everything here
   */
  resetAll() {
    this.chartData = [];
    this.question = '';
    this.totalVotes = 0;
  }

  /**
   * Create the chart data, by adding the answer added in the question-answer grid to the labels
   * set the initial count of votes for each answer to 0
   * @param answer - the answer added at the question component
   */
  updateChartData(answer: string) {
    if (!this.chartData.find(data => data.label == answer))
      this.chartData.push({ label: answer, count: 0 });
  }

}
