import { ComponentFixture, TestBed } from '@angular/core/testing';
import { chartData, updatedChartData } from '../../mock-data/test-data';

import { ChartComponent } from './chart.component';

describe('ChartComponent', () => {
  let component: ChartComponent;
  let fixture: ComponentFixture<ChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('remove answer from chart data', () => {
    component.chartData = updatedChartData;
    component.removeFromChartData('3.30');
    fixture.detectChanges();
    expect(component.chartData).toEqual(chartData);
  });

  it('should reset everything on reset', () => {
    component.resetAll()
    expect(component.chartData).toEqual([]);
    expect(component.totalVotes).toEqual(0);
  });

  it('should not remove anything if an invalid answer is asked to be removed', () => {
    component.chartData = updatedChartData;
    component.removeFromChartData('5.15');
    expect(component.chartData).toEqual(updatedChartData);
  });

  it('should not add duplicate labels', () => {
    component.chartData = updatedChartData;
    component.updateChartData('3.14');
    expect(component.chartData).toEqual(updatedChartData);
  });

  it('should unsubscribe to events ', () => {
    spyOn(component['resetEventSubscripton'], 'unsubscribe');
    spyOn(component['answerAddEventSubscription'], 'unsubscribe');
    spyOn(component['answerRemoveEventSubscription'], 'unsubscribe');
    spyOn(component['voteSubscription'], 'unsubscribe');
    component.ngOnDestroy();
    expect(component['resetEventSubscripton'].unsubscribe).toHaveBeenCalledTimes(1);
    expect(component['answerAddEventSubscription'].unsubscribe).toHaveBeenCalledTimes(1);
    expect(component['answerRemoveEventSubscription'].unsubscribe).toHaveBeenCalledTimes(1);
    expect(component['voteSubscription'].unsubscribe).toHaveBeenCalledTimes(1);
  })

  it('should add labels when answer is added in the question component', () => {
    component.chartData = chartData;
    component.updateChartData('3.30');
    fixture.detectChanges();
    expect(component.chartData).toEqual(updatedChartData);
  });

});
