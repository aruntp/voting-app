import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {

  question: string = '';
  answers: string[] = []
  constructor() { }

  /**
   * handler for questionAdded event from question component, 
   * which is then shared to child components
   * @param question question added from Question component
   */
  questionAddedHandler(question : string) {
    this.question = question;
  }


}
