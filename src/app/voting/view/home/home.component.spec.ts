import { ComponentFixture, TestBed } from '@angular/core/testing';
import { answers, updatedAnswers } from '../../mock-data/test-data';

import { HomeComponent } from './home.component';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should remove answer on answeradd event', () => {
    component.answers = updatedAnswers;
    component.answerRemoved('3.30');
    expect(component.answers).toEqual(answers);
  });

  it('should add answer on answeradd event', () => {
    component.answers = answers;
    component.answerAdded('3.30');
    expect(component.answers).toEqual(updatedAnswers);
  });

  it('should update question on questionadded event', () => {
    component.questionAdded('test question?');
    expect(component.question).toEqual('test question?');
  });

});
