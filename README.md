# VotingApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.1.2.

# Usage Guide

This application is used for voting, User can create question for vote, add upto 10 different answers, and view the reponse in a bar chart.

```
## Question
    * A text box at the top left on the first column is given to add the question for voting. The text box for providing answer will be visible
        after entering some text in the question field. 
    * 'Reset' button is provided to reset the entire UI, it clears all the answers, chart and the question
    * The poll question can be changed, and the answers can also be removed or added. 

## Answers 
    * User can add upto 10 different answers.
    * After providing the answer on the text field, click on the 'Add' button to add it for voting
    * Each question can be deleted using the 'x' button next to it
    * The chart with results will be set up when the answers are added on the question component

## Voting
    * Use the second column to cast the vote.
    * The 'Vote' button is visible, only if there are at least 2 answers.
    * To cast a vote, select a radio button to the left of the answer and click 'Vote'. 
    * Multiple answers can be submitted by a user
    * The count and graph will update as soon as the Vote button is pressed
    * The legend and count are marked for each answer

## Chart
    * A simple bar chart is used to show the vote response
    * The legend and count are marked for each answer, with the bar
    * The bar for each answer is updated immediately after the 'Vote' is done
    * The total votes polled is shown at the bottom

```

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Deployment URL

The application is deployed and can be accessed through - `https://voting-app-b21bd.web.app/` . 

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
